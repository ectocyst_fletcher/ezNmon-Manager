package tdd.xuchun.test.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import tdd.xuchun.test.model.Host;
import tdd.xuchun.test.model.Page;
import tdd.xuchun.test.model.Result;
import tdd.xuchun.test.service.IHostService;

/**
 * 主机控制器
 * 
 * @author ZSL
 *
 */
@RestController
@RequestMapping("host")
public class HostController{
	
	/**
	 * 注入主机业务服务
	 */
	@Autowired
	private IHostService hostService;

	
	/**
	 * 查询单个主机详细信息
	 * 
	 * @return
	 */
	@RequestMapping("/editpage")
	public ModelAndView findHost(Host host){
		Host rsHost = hostService.getHost(host);
        Map<String,Object> rsMap = new HashMap<String,Object>();
        rsMap.put("data", rsHost);
		return new ModelAndView("host_edit",rsMap);
	}
	

	/**
	 * 查询单个主机详细信息
	 * 
	 * @return
	 */
	@RequestMapping("/copyaddpage")
	public ModelAndView findAddHost(Host host){
		Host rsHost = hostService.getHost(host);
        Map<String,Object> rsMap = new HashMap<String,Object>();
        rsMap.put("data", rsHost);
		return new ModelAndView("host_copyadd",rsMap);
	}
	
	/**
	 * 增加新主机
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public Result addHost(Host host){
		hostService.addHost(host);
		return new Result();
	}
	
	/**
	 * 删除主机
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	public Result delHost(Host host){
		hostService.delHost(host);
		return new Result();
	}
	
	/**
	 * 编辑主机
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	public Result editHost(Host host){
		hostService.editHost(host);
		return new Result();
	}
	
	/**
	 * 模糊查询主机列表
	 * 
	 * @return
	 */
	@RequestMapping("/getcond")
	public Page queryLikeHosts(int pageSize,int pageNum,Host host){
		Map<String,Object> cond=new HashMap<String,Object>();
		cond.put("pageSize", pageSize);
		cond.put("pageNum", pageNum);
		cond.put("host", host);
		Page page = hostService.queryLikeHosts(cond);
		return page;
	}


	
}
